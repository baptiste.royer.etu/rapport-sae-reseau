Rapport Final SAE de Réseau
===========

Auteurs du rapport : MIHOUBI Jenna, ROYER Baptiste, NOISETTE Thomas
Contacts : <jenna.mihoubi.etu@univ-lille.fr>, <baptiste.royer.etu@univ-lille.fr>, <thomas.noisette.etu@univ-lille.fr>

# Conversion du fichier MarkDown avec Pandoc

Afin de convertir le fichier MarkDown, il suffit de taper les commandes suivantes dans un terminal : 

```bash
pandoc Rapport_Final_SAE.md -o Rapport_Final_SAE.pdf
```
Permet la conversion en format PDF.

```bash
pandoc --standalone --template template.html Rapport_Final_SAE.md --metadata title="Rapport_Final_SAE.html" -o Rapport_Final_SAE.html
```
Permet la conversion en format html, avec feuille de style.

(La feuille de style a posé problème pour le pdf, c'est la raison pour laquelle la commande pour le format pdf est plus courte.)


# Dans le cas où vous n'avez pas les commandes pandoc, entrez cette suite de commande :

```bash
sudo apt-get install pandoc
```
Permet l'installation du package pandoc.

```bash
sudo apt-get install textlive-latex-base textlive-fonts-recommend textlive-latex-extra
```

pandoc --standalone --template template.html rapport.md --metadata title="Rapport.html" -o Rapport.pdf