﻿# Rapport technique intermédiaire.

**Sommaire** :
-

1. [**Préparation de la machine virtuelle**](#1)

[- Préparation de la machine virtuelle.](#1.1)
[- Installation de l'OS.](#1.2)

2. [**Préparation système.**](#2)

[- Accès sudo pour *user*.](#2.1)
[- Installation des suppléments invités.](#2.2)

3. [**À propos de la distribution Debian.**](#3)

[- Documentations.](#3.1)
[- Quelques questions.](#3.2)

4. [**Installation préconfigurée.**](#4)

[- Récupérer et préparer les fichiers nécessaires.](#4.1)

5. [**Analyse préliminaire de git et des outils graphiques associés.**](#5)

[- Configuration globale de *git*.](#5.1)
[- Interfaces graphiques pour *git*.](#5.2)
[- Installons autre chose et comparons.](#5.3)
[- Essais à faire.](#5.4)
[- Comparaisons.](#5.5)

6. [**Redirection de port.**](#6)

[- Redirection de port.](#6.1)

7. [**Installation de *Gitea*.**](#7)

[- Quelques questions.](#7.1)
[- Installation “from binary” de *Gitea*](#7.2)
[- Mise à jour du binaire du service *Gitea*](#7.3)


# Préparation de la machine Virtuelle :**<a id="1"></a>**


**Préparation de la machine virtuelle :<a id="1.1"></a>**
-
1. Que signifie “64-bit” dans “Debian 64-bit” ?
> "64-bits" signifie que Debian est conçu pour fonctionner sur des processeurs capables de traiter des instructions en 64 bits.

2. Quelle est la configuration réseau utilisée par défaut ? 

> La configuration réseau par défaut est la configuration NAT (Network Address Translation).

3. Quel est le nom du fichier XML contenant la configuration de votre machine ? 

> Il s'agit du fichier : sae203.vbox-prev

4. Sauriez-vous le modifier directement ce fichier de configuration pour mettre 2 processeurs à votre machine ?

> Il doit y avoir une balise qui sert dans ce but, mais ce n’est pas recommandé.
  

**Installation de l'OS :<a id="1.2"></a>**
-
6. **Qu'est-ce qu'un fichier iso bootable ?**

>Oui, il est important de savoir ce qu'est un fichier ISO bootable pour l'installation de Debian. C'est une image disque complète d'un CD ou DVD amorçable qui peut être gravée sur un support physique (comme une clé USB ou un DVD) et utilisée pour démarrer un ordinateur et installer un système d'exploitation.

2. **Qu'est-ce que MATE ? GNOME ?**

>Oui, il est important de connaître les différences entre les environnements de bureau MATE et GNOME pour choisir celui qui convient le mieux à vos besoins.
>-   MATE: MATE est un environnement de bureau stable, léger et personnalisable, avec une interface utilisateur traditionnelle.
>-   GNOME: GNOME est un environnement de bureau moderne et convivial, avec une interface utilisateur intuitive et des fonctionnalités avancées.
    

3. **Qu'est-ce qu'un serveur web ?**

>Oui, il est important de comprendre le fonctionnement d'un serveur web pour configurer et administrer votre propre serveur. Un serveur web est un logiciel qui distribue des fichiers web aux clients qui en font la demande via un navigateur web.

4. **Qu'est-ce qu'un serveur ssh ?**

>Oui, il est important de connaître les avantages d'utiliser un serveur ssh pour sécuriser vos connexions à distance. Un serveur ssh permet de se connecter à un ordinateur distant de manière sécurisée en utilisant le protocole SSH.

5. **Qu'est-ce qu'un serveur *mandataire* ?**

>Oui, il est important de comprendre les avantages et les inconvénients d'utiliser un serveur mandataire pour améliorer la sécurité et la confidentialité de votre navigation web. Un serveur mandataire agit comme un pont entre votre ordinateur et Internet et peut être utilisé pour filtrer les contenus web, masquer votre adresse IP et améliorer la performance de votre navigation.

# Préparation du système :<a id="2"></a>

**Accès sudo pour *user*:<a id="2.1"></a>**
-

1. Ajout du groupe sudo au groupe principal : 

> adduser user sudo ajoute user au groupe sudo

2. Comment peut-on savoir à quels groupe appartient l'utilisateur *user* ?

>usermod -aG sudo <nom_utilisateur> pareil mais à partir du compte root



4. **Comment peut-on savoir à quels groupes appartient l’utilisateur user?**

> groups user

Installation des suppléments invités :<a id="2.2"></a>
-
1. Quelle est la version du noyau de Linux utilisé par votre VM ?
> Commande utilisée : uname -r
> Ici c’est la version 6.1.0-17-amd64

2. À quoi servent les suppléments invités ? Donner 2 principales raisons de les installer.

	>1.  Améliorer les performances: Les suppléments invités installent des pilotes de périphériques et d'autres logiciels qui peuvent améliorer les performances de votre machine virtuelle, notamment la vitesse de la souris, le partage du presse-papiers et la gestion de la résolution d'écran.   
	>2.  Accéder à des fonctionnalités supplémentaires: Les suppléments invités peuvent vous donner accès à des fonctionnalités supplémentaires, telles que la possibilité de partager des dossiers entre votre machine virtuelle et votre ordinateur hôte, ou de synchroniser l'heure de la machine virtuelle avec l'heure de votre ordinateur hôte.
3. À quoi sert la commande *mount* ? (Dans notre cas de figure et dans le cas général).
	>1. Dans notre cas de figure :
La commande mount est utilisée pour monter le CD des suppléments invités sur votre machine virtuelle. Cela permet d'installer les suppléments invités. 

	>2. Cas général :
La commande mount est utilisée pour associer un périphérique de stockage à un point de montage dans le système de fichiers. Cela permet d'accéder aux fichiers et dossiers du périphérique de stockage.

	>Exemples :
	>-   Monter un CD-ROM :
	>```bash
	>sudo mount /dev/cdrom /mnt/cdrom
	>```
	>-   Monter une partition USB :
	>```bash
	>sudo mount /dev/sdb1 /mnt/usb
	>```

# À propos de la distribution Debian<a id="3"></a>

Documentation :<a id="3.1"></a>
-

-   debian.org/doc/ : Le site officiel de Debian propose une documentation complète et traduite dans de nombreuses langues, y compris le français.
    
-   debian-fr.org : Portail francophone avec des guides, des tutoriels et des forums pour vous aider à utiliser Debian.
    
-   forum-debian.fr : Forum francophone où vous pouvez poser vos questions à propos de Debian et obtenir des réponses d'autres utilisateurs.
    
-   debian-facile.org : Site avec de la documentation en français sous forme de wiki et un forum.
    

Quelques questions :<a id="3.2"></a>
-

1. Qu'est-ce que le Projet Debian ? D'où vient le nom Debian ?

>   Le Projet Debian est une communauté mondiale de développeurs bénévoles qui s'occupent de la distribution GNU/Linux Debian. Le nom "Debian" est un mot-valise composé des prénoms de Ian Murdock, le fondateur du projet, et de sa femme Debra.
    

2. Quelle sont les durées des 3 prises en charge de ces versions ?

>-   Durée minimale : 1 an 
>-   Durée en support long terme (LTS) : 5 ans   
>-   Durée en support long terme étendue (ELTS) : 5 années supplémentaires après la fin du support LTS (soit 10 ans)
    

3. Pendant combien de temps les mises à jour de sécurité seront-elles fournies ?

>   Les mises à jour de sécurité sont fournies pendant la durée de prise en charge de la version. Pour les versions LTS et ELTS, les mises à jour de sécurité sont disponibles pendant 10 ans après la date de publication de la version.
    

4. Combien de version au minimum sont activement maintenues par Debian ?

>   Au minimum 3 versions sont activement maintenues :
>-   La version stable actuelle (Bullseye)
>-   La version "oldstable" (Buster)
>-   La version "oldoldstable" (Stretch)
    

5. D’où viennent les noms de code données aux distributions ?

>   Les noms de code des distributions Debian sont tirés des personnages de la série de livres et films Toy Story.
    

6. Combien et quelles d'architectures sont prises en charge par la version Bullseye ?

>   Bullseye prend en charge 11 architectures, dont amd64, i386, arm64, armel, powerpc64el, s390x et sparc64.
    

7. Première version avec un nom de code :

-   Premier nom de code : 
> Buzz
    
-   Date d'annonce : 
>16 août 1996
    
-   Numéro de version de cette distribution :
> 1.1
    

8. Dernier nom de code attribué :

-   Dernier nom de code : 
> Bookworm
    
-   Date d'annonce :
> 30 juillet 2022
    
-   Numéro de version :
> 12
    
# Installation préconfigurée <a id="4"></a>

Récupérer et préparer les fichiers nécessaires :<a id="4.1"></a>
-
1. Ajout du droit d'utilisation de *sudo* par l'utilisateur principal

>#d-i passwd/user-default-groups string audio cdrom video sudo
(juste ajouter sudo a la liste des groupes)

  

2. Installer l’environnement mate

> Modification du fichier preseed-fr.cfg :

![](https://lh7-us.googleusercontent.com/YmntuGIqUp8hWl48B9IfNUhSBRJOfrOo0qUCYFkMjT58DkDy5h-oUphe7B_ofb1G0nt5mTk4wPEvqTNm5_9jycHSGYT4kqc0GyHfCMlfqOaKp9XDC8_yT9Uw2wzXReRzQOtmpYwo0QjcShF1wORm6fE)

3. Ajout des package sudo, git, sqlite3, curl, bash-completion et neofetch au fichier de configuration automatique (pressed-fr)
> Modification du fichier preseed-fr.cfg

![](https://lh7-us.googleusercontent.com/5sCTrmJrXfSQzFxgh3QZt5yRzc50wIy9FDt2Tlsxpyq-SjtaNtoLHgUnsU8zv1hXOwUOYKzMqgWEyk7jAhq9DCib4qilqe56V8TGxCBn7cD84F_4rSa0cVAiwX42ymCdYIUkkLKh9Kz4P6zx-pYAZG0)

# Analyse préliminaire de git et des outils graphiques associés :<a id="5"></a>

**Configuration globale de *git* :**<a id="5.1"></a>
-
1. Explication des lignes suivantes : 
- git config --global user.name "Prénom Nom"
- git config --global user.email "votre@email"
- git config --global init.defaultBranch "master"

> Code permettant de configurer notre nom d'utilisateur, notre adresse email et la branche par défaut pour Git pour que nos commits soient correctement identifiés :




**Les interfaces graphiques pour *git* :**<a id="5.2"></a>
-
1. Que sont les logiciels gitk et git-gui ?

> Gitk et git-gui sont deux outils graphiques puissants pour visualiser et gérer l'historique de votre dépôt Git.

2. Comment les installe-t-on ?
```bash
sudo apt-get install gitk
sudo apt-get install git-gui
```

3. Comment les lance-t-on ?
```bash
gitk
git-gui
```

4. Que permet *gitk* ?

>-   Explorer l'arborescence des commits de votre projet.
>-   Visualiser les modifications apportées aux fichiers à chaque commit.
>-   Comparer différentes versions d'un même fichier.
>-   Filtrer les commits par auteur, date, message, etc.
>-   Accéder aux informations détaillées de chaque commit.
    

5. Que permet *git-gui* ?

>-   Créer des commits en sélectionnant les fichiers modifiés.
>-   Ajouter un message de commit clair et précis.
>-   Annuler les modifications apportées aux fichiers.
>-   Créer et gérer des branches.
>-   Fusionner des branches entre elles.
    

6. Quels sont les avantages des outils graphiques utilisés ?

>-   Facilité d'utilisation : Ils offrent une interface intuitive et conviviale, accessible aux utilisateurs débutants.
>-   Visualisation claire : Ils permettent de visualiser l'historique du projet de manière graphique, ce qui facilite la compréhension des modifications apportées.
>-   Gain de temps : Ils automatisent certaines tâches répétitives, comme la création de commits ou la fusion de branches.
    

7. Quels sont leurs inconvénients ?

>-   Moins puissants que les commandes en ligne de commande : Ils ne proposent pas toutes les fonctionnalités disponibles avec Git.
>-   Dépendance d'une interface graphique : Ils ne peuvent pas être utilisés sur des serveurs sans interface graphique.
    
**Installons autre chose et comparons :**<a id="5.3"></a>
- 

1. Sur quel logiciel s'est porté votre choix ?

> Notre choix s'est porté sur le logiciel Magit.

2. Pourquoi ce choix ?

> Il est l’une des interfaces gratuites les plus utilisées, de ce fait la communauté est active ce qui nous facilitera si besoin. 
> De plus, l'interface est puissante, flexible et personnalisable : Magit permet de gérer facilement les remotes, en les clonant, les synchronisant et en y “poussant” les modifications. 
> On peut également y voir l’historique complet de nos dépôts avec des options et filtres de recherche.


3. Comment l'avez vous installé ?
```bash
sudo apt update
```
```bash
sudo apt-get install magit
```
4. Comment l'avez vous lancé ?
```bash
emacs
```
> une fois dans l'application, il faut déclencher l'apparition du terminal avec la combinaison de touches :
>  Alt + x.
```bash
magit
```
**Essais à faire :**<a id="5.4"></a>
-
Prérequis : dépôt git local

  

Essai avec des documents personnels : emacs <nom_fichier>

  
1. Comment avez vous fait pour accéder directement à un répertoire git en ouvrant Magit ?

>- Étape 1: Après la création d’un répertoire « test » contenant les fichier test.txt et textdiff.txt, on lance emacs avec la commande « emacs ».
>
>- Étape 2: Activer Magit (Avec Alt + x) :
>```bash
>M-x magit
>```
>![](https://lh7-us.googleusercontent.com/X4FnWcbcP37HLXLIHLA9yGykqYlByDz1_SWsgG4qxMEK9T9b9JV-hx5Iy_tsPYR3xHJ--xcv75mHjKS6DjLA6BoH8Zg44cPkGX1SWqmNCwmp1x7Yi2YpWvInoO3DEbNO_1bXpNepazbaUrEhu4hgMAo)
>Accès au répertoire “test” dont le message de commit était test magit.

  
2. Comment avez vous fait pour accéder à l'historique du répertoire git ?
  
>Étape 1: En s’aidant des flèches droites et gauches, déplacer le curseur sur la branche qu’on veut explorer et appuyer sur entrée : 
>
>![](https://lh7-us.googleusercontent.com/T3yLGuz7vCnf-T7Ga3uFqyQo2Tj5Oi2lhiqxe3q0jOpXLMhQQiM8LDgP8xgyWoCRYxM_NGFCuBGQsuupbcNa2P6m5yRJ9HZedYdnmT_VfTWvjcwfbn_ZgZsZvdN0teD0-f3PJEyokz0doJqgp4aUXlM)
>
>Étape 2: A l’aide des flèches haut et bas, naviguer dans l’historique des versions et créations de fichier :
>
>![](https://lh7-us.googleusercontent.com/OybviItT5icnICIHyf43YKcnw0HsRviYBBrlQH3bSFZr8gcC__jhjr3-iJjt8Yl-Jp7ObyOjQXXhm7mm0tkEAFiHZGOXy8DmKxT89E0LZWalUO1Bwtpd5xVDlv6CvYIbnJCr-XkxgsvUqSqbY8Mw5ec)
>
>![](https://lh7-us.googleusercontent.com/IBY-NyNTUG2t_3RE2tuh4vhqM70CJX8wiGPxk7EezI2BL9yB3v-zpDAsyk8U22SlTTUI0aybyoFrz9qf6IMZFSVgpjW0zqxM0e2883NfLOC49_LQ5DODEKvvWMDVw-jkuZsv5dURAIfa5dGaCyYGAmA)

3. Comment peut on comparer deux versions d'un fichier ?

>Étape 1: Modifier le fichier

>Étape 2: Ouvrir le fichier dans Emacs
>```bash
>emacs <nom_fichier>
>```

>Étape 3: Activer Magit (Avec Alt + x) :
> ```bash
>M-x magit
>```

>Étape 4: Afficher les différences (Avec Alt + x) :
>```bash
>M-x magit-diff
>```

>Étape 5: Sélectionner les versions à comparer :
> - Utilisation des touches fléchées
> - Commande : ```M-x magit-log```
>
>Avec ces deux méthodes, nous pouvons afficher l'historique complet du fichier :
>
>![](https://lh7-us.googleusercontent.com/k6AjgFxGAFN6niJWho31SL2HJJyolXetRhW1hqKdkx3h4sLXiKokYTdhLPaAsF7TZSnNCqF-VwePxlJ1GroezfgWwTa_HeJYpmD4tU39l-IqOzCP-dxGFLOgJCRkBWvCU2rNF4SV63ivdk7SvVOt3sQ)

  
  

4. Comment vous êtes vous pris pour créeret supprmier des branches ?

  

>Étape 1: Créer une branche (Avec Alt + x) :
>```bash
>M-x magit-branch-create
> ```
> On l’appelle ‘nouvelle branche’.

>Étape 2: Basculer vers une branche (Avec Alt + x) :
> 
>```bash
>M-x magit-checkout
>```
> ```bash
> nouvellebranche
> ```
>![](https://lh7-us.googleusercontent.com/P2NkmWyGdCgBQ5FsBMCqnv6QhsnXtmE4NzvXGSdWE6f0i9yw7CyDa3Zzm_l0cjCVH5ctnn7E_R4hWahkXpzoaMkIi9uYyf5tKL7nayY9BAz8Q1zHJd-O7i-rr0a5vuqWZoPGvSaeDNJ0cPiuWX1mcDg)

### Comparaison : <a id="5.5"></a>

  


|Fonctionnalité |Gitk | Git-gui | Magit |
| :------------------:| :------------------ | :-------------| :-----------------|
| **Avantages** | Possède une interface graphique simple et intuitive. Permet de visualiser l'historique du projet. Offre des options de filtrage et de recherche. | Possède une interface graphique simple et intuitive. Permet de créer et de modifier des commits. Facilite la gestion des branches.Offre des options de fusion et de rebase. | Possède une intégration native avec Emacs. Offre un large éventail de fonctionnalités pour visualiser et gérer l'historique du projet. Permet d'effectuer des actions complexes sur les commits et les branches.Offre une interface personnalisable. |
| **Inconvénients** | Possède des onctionnalités limitées. Ne permet pas de créer ou de modifier des commits. N'offre pas d'options de collaboration. | Possède des fonctionnalités limitées comparées à Magit. Ne s'intègre pas avec d'autres outils de développement. Possède une interface moins personnalisable que Magit. | Possède une courbe d'apprentissage plus raide. Nécessite une configuration d'Emacs. Ne fonctionne pas sur tous les systèmes d'exploitation. |
| **Collaboration** | Non | Non | Oui |
| **Intégration avec d'autres outils** | Non | Non | Oui |
| **Coût** | Gratuit | Gratuit | Gratuit |
| **Licence** | Open-source | Open-source | Open-source |

# Redirection de port :<a id="6"></a>
**Redirection de port :**<a id="6.1"></a>
-
### Montrez comment vous êtes parvenus à rediriger les ports :

>-   Étape 1: Se rendre dans les paramètres de la virtualbox :![](https://lh7-us.googleusercontent.com/oMW3XvFZKFgnXQjNqM2c9DM5TT2cD5nEGqoL5-pY8PK8R8_v0c1X8HqEMOL7ZvAZdrp_Pjcf5nXmM-7TJpni71ysdhh7UkP0zVj0TWWbb7Lc1e1Izj_VgxHuguqNDGKAacPaVffxlUtU11m9nUBjRIc)
    
>-   Étape 2: Se rendre dans la section Network, déplier le menu avancé :![](https://lh7-us.googleusercontent.com/5X0f9uuN_riqEUwJUiJnyl7NP-nNOzGMpVkQWkjPejaDqxZOmel-nRiVRk9puNHQsmWD-jnSACCKq9lhtjhXN0IgnnAkRhWY6vCffB15DH_RpfsdHwTMTw0CUTYgCTHb-VrGct_vA2pu6NBJfja323Q)
    
>-   Étape 3: Sélectionner port forwarding et remplir les champs Host Port et Guest Port par 3000 puis valider :![](https://lh7-us.googleusercontent.com/6kVAvd2teYOqWjfUtmyNf7ripsnDU4IhlantqULcxItdArz1M9RRoL4qWSllKzQaeqqyBFkLBSCUFSIQWAlTtBQWBhyKCdnxf-pNyBGoXKjt_p2vnp4ozQfnUFyW575HXHAYRbjqPkTgT2yggYeEqMY)![](https://lh7-us.googleusercontent.com/jb-Vq3Y0PcbiF5ojvA8kZUYA9iNU5YhAFfYnRya3NJe4d0KPchyvR93hokw8YhAw5r_Z4rU0dcbGGV-LkkbTwiQ4wuInMff1d0uThd9rx9uOKJUWBo7Hwr_Szx9HVP30FzHyCBGeGsVxebZQjqjf2V8)![](https://lh7-us.googleusercontent.com/Ov4LRKo9BXaGs2YPb-dFGveXvB017BVZyRtF_OtvFUo6HMuaPacIDnGKuY6U7eaZzQ7GTbhzvaopxOcEclgFX9rQCNLgr1kBgc-g3_czTEh4x_TYSH9-11gLWX0a2_xLAQDauwp2ypDuyZ_jghYsiE0)
>
>Le port 3000 est choisi car Gitea utilise par défaut le port 3000 pour se lancer. Une fois Gitea installé, nous n’aurons plus besoin de systématiquement lancer la machine virtuelle. Nous pourrons y accéder, par exemple, grâce à l’url :  http://localhost:3000.

>Il ne nous reste donc plus maintenant qu’à installer le service Gitea sur notre machine virtuelle.


# Installation de *Gitea* :<a id="7"></a>
**Quelques questions :**<a id="7.1"></a>
-

1. Qu'est-ce que Gitea ?
    
>Gitea est un logiciel de gestion libre de dépôt Git (versions de code source) auto-hébergé, il permet d’héberger de gérer les dépôts git en plus de présenter des fonctionnalités telles que le suivi des problèmes, la gestion des versions et l’intégration continue.

2.  À quels logiciels bien connus dans ce domaine peut-on le comparer (en citer au moins 2) ?

> Nous pouvons comparer Gitea a GitHub ou encore GitLab. GitHub. GitHub: Le service de gestion de versions de code source le plus populaire. GitLab : Un autre service de gestion de versions de code source open source (tout comme gitea).

>[Source sur la définition de Gitea][Gitea - Bienvenue sur le site consacré à l'utilisation de Linux ! - Wiki (univers-linux.fr)](https://www.univers-linux.fr/wiki/gitea#:~:text=Gitea%20est%20un%20logiciel%20libre%20de%20gestion%20de,probl%C3%A8mes%2C%20la%20gestion%20des%20versions%20et%20l%27int%C3%A9gration%20continue.)
>
>[Comparaison entre Gitea et GitHub][Gitea vs GitHub | What are the differences? (stackshare.io)](https://stackshare.io/stackups/gitea-vs-github)
>
>[Comparaison entre Gitea et GitLab][Gitea VS Gitlab (hostedgitea.com)](https://hostedgitea.com/article/gitea-vs-gitlab)

  

**Installation “from binary” de *Gitea* :**<a id="7.2"></a>
-

>   - Étape 1: Télécharger le fichier avec wget :
>
>Commandes pour télécharger Gitea 1.21.8 pour 64-bit Linux :
>```bash
> wget -O gitea https://dl.gitea.com/gitea/1.21.8/gitea-1.21.8-linux-amd64
> ```
>```bash
>chmod +x gitea
>```
>
>[https://dl.gitea.com/gitea/1.21.7/gitea-1.21.7-linux-amd64.xz](https://dl.gitea.com/gitea/1.21.7/gitea-1.21.7-linux-amd64.xz)

>Téléchargement réussi :
>
>![](https://lh7-us.googleusercontent.com/dvusXfi3SGyyY7goA_TIAuI62RTIrebv-YeQ1yupRMTDrKVqo8rZzJb0pxbHnmbexCi-ox-odB5-nY4jtV0VSCzQXmDzdt0gVfsTCqoRTqT5SLUNaJRgpEiV2dIOZ6QKn2PQwVn1xAokZ6nSUtEBwjg)

  

>-   Étape 2: Vérifier la signature GPG (facultatif) :
>
> Gitea signe ses binaires, pour vérifier la signature nous pouvons télécharger le fichier de signature (finissant par .asc) et utiliser l'outil en ligne de commande gpg.
> 
>Pour ce faire, nous pouvons installer gpg :
> ```bash
> sudo apt install gpg
> ```
>  
> Puis utiliser la commande :
> ```bash
> gpg --verify fichier.sig
> ```
  

> Si la phrase : Good signature from "Teabot <[teabot@gitea.io](mailto:teabot@gitea.io)>" s’affiche alors la signature est bonne.
>
> [ *Nous n’avons pas réussi cette étape car échec lors de l’ouverture du fichier : .asc* ]

> -   Étape 3 : Vérifier la version :
>
> Grâce à la commande : 
>  ```bash
>  git --version
> ```

> Il faut que la version soit la 2.0 ou supérieure.
> 
>![](https://lh7-us.googleusercontent.com/cr5NPn7R4IdmvkpphpLrwQjSEfEJzWrq7CyR68RK215jkwKOugfEV0yo020Iz5PzCZBXideBuGsWo4bFgN0YQDI6-H8Phc4amkB5PiGD9GVcx6uC6HPC2jljuZXH4qIswOgB258KwcuKilh1_J5Jkhk)
>
> Nous constatons que nous sommes sur la version 2.39.2 de git sur notre machine virtuelle.

>-   Étape 4: Créer un utilisateur (user) :
> ```bash
>sudo adduser --system --no-create-home --shell /bin/bash gitea
>```
>
>![](https://lh7-us.googleusercontent.com/jv3XnliNUjW-YgxVKxWDgLleWu2c0XrlueoHjBbAf4EkWZQ82D921uMA89Qj14pSoxrwmU0WbDu-gRvmEwvyCqidAggpSQsarDBS7j6CPp72CU6aZ48EWpCch281CUSdJ3WhN2h5ONltyFxxTr9id1U)
>
>![](https://lh7-us.googleusercontent.com/ntclX9aV6G4O_R6-rVYynL1egHQGwD_uJXDV8qJ-1U7q6p39weF-V_QJ9_jgka4znOGboh7-4OxCKUyGPFwNBJPng3Kiz2MxkpEL8aGXJ76wXA-aM-P4D9c5Hj7oxZiz83rvadlSIiKlLOJElxVtRPo)

  

>-   Étape 5: Créer une structure de répertoire requise :
>
>```bash
> mkdir -p /var/lib/gitea/{custom,data,log}
> ```
> ```bash
> chown -R gitea /var/lib/gitea/
> ```
> ```bash
> chmod -R 750 /var/lib/gitea/
> ```
> ```bash
> mkdir /etc/gitea
> ```
> ```bash
> chown gitea /etc/gitea
> ```
> ```bash
> chown root /etc/gitea
> ```
> ```bash
> chmod 770 /etc/gitea
> ```

> -   Étape 6: Copier le Gitea binaire dans une location globale
>```bash
> cp gitea /usr/local/bin/gitea
>```
> 
  

>-   Étape 7: Lancer Gitea manuellement :
>```bash
> GITEA_WORK_DIR=/var/lib/gitea/ /usr/local/bin/gitea web -c /etc/gitea/app.ini
> ```
>
> [ *Nouvelle difficulté rencontrée : erreur inconnue.* ]
>
>![](https://lh7-us.googleusercontent.com/VTlQ62nFSNtAjjutAvJLBzOLPQHF7G51wW1T4n927kpZAf3gh0kw1kxWKTIP-bamo_gtLQ51nV6GLCdpNogzQDwusOoaxsao2hobATmGGtr1pyDqpSx_PbaG7QcX3zPUjVtxUGNZse_40hYy3zZS6eo)

>D’après la documentation officielle de gitea, /etc/gitea  est temporairement installé avec les droits d'écriture sur l'utilisateur que nous avons créé (ici gitea et root). Il est recommandé de retirer ce droit et de ne le laisser qu’en lecture seule après l’installation :
>```bash
>chmod 750 /etc/gitea
>``` 
>```bash
>chmod 640 /etc/gitea/app.ini
>```

> [ *Ça ne fonctionne toujours pas, en effet, il ne faut pas lancer git à partir de l’interface root. Nous allons donc essayer de la lancer à partir de la session utilisateur.* ]
>
>![](https://lh7-us.googleusercontent.com/aKow6FzkLPcWbueJCj4ZhFV0f-NiOkG2FIJiPuCtRdBoxTvEEoQo7gziBOBAAeg1L1wRMeC0FELxjQUd18pxxWCUHzRTweo4LcP8obZu-jN-1ea4b51lQSMj1FF8Vnq7vrp4OV5rn3HBxuCMbVubtnE)
>
> [ *Nous essayons de remettre les droits sur app.ini à l’utilisateur en question :* ]
>```bash
>sudo chmod 770 /etc/gitea/
>```
> Remettre les droits d'exécution :
> ```bash
> sudo chmod +x /etc/gitea/
>```
>
>![](https://lh7-us.googleusercontent.com/VR1vmzVlspZsOkWwlKVvTGLaf9_Ck1CKjSWpgDwDI9a7U66kXPsuz1Qn3JDg_XVZC1yrrbprwHTJQPdbTpEmDHqPDidgbf1YQJr8_LS6dx8NMCa-oVIoCGGlAwK7LXqomN0nB9gTPEvbNIg2CScgfMk)
>
> [ *Après plusieurs manipulation des droits, la page reste figée comme suit :* ]
>
>![](https://lh7-us.googleusercontent.com/rUsreO00ZLxJrMA06lMdOlbFM3cYb6m7P_rsoi7dtlOWMe1fWYetOEE0zC67z_ErkwzExc16PuvnKZESALUNkIRcrWzrRTJj2nzbGq3FnCvAtZKZjdjGvTWO8Lta7Dkj411RLshZRqERV01skHK-J4o)
>
> [ *Cela signifie que la commande a fonctionné, on peut désormais accéder via le port 3000 de la machine virtuelle :* ]
>
>![](https://lh7-us.googleusercontent.com/p0aXf_147nRt_qTEFbSL5HvRyCytmo5Z-JBEhogLbJthre8B4xgZh068WQAREQmHb-pyQxu8hwibnVzrxHvA-7iAXT8l-aJcxdKIlsNDrhqf-_2lsP7MOGr3CejpWn9eIHIfoCr_Uh04FGvh_Zvhxgk)
>
> [ *La redirection du port 3000 de la vm vers le port 3000 de la machine hôte ne fonctionne pas :* ]
>
> ![](https://lh7-us.googleusercontent.com/TruQEX3I9t0kfvudQnpZSkksfjsInu17NCy4O-f52iphjH0PgqcDScIE4ZVL05UNdQTU4rY2A5QIq6ihMfN5L88hxwo_FWmYvJKVw7CQCkhp1q2QogEzdtPpX8-L95U6Xdfs7arnReOgu86EWYGEw4I)
>
> [ *Création du compte administrateur dont le mot de passe est gitea :* ]
>
> ![](https://lh7-us.googleusercontent.com/rcNdfJDdeUoK1Ak_8EQRnfZR-oxGeo8QBvSkE12uZkq5tB6GUrBbfyYtP-OU5W_7aYAYVB4_AHJQlk_AS22vF0-oyyGhN96HpCaZ2m6ivlbR7NXyqrlVWCW2c6ZF4dXuZPMeyAFznTOc7awF4jk00S0)
>
> [ *Lancement l’installation à partir de sqlite3, mais cela ne fonctionne pas :* ]
> ![](https://lh7-us.googleusercontent.com/CgAKq68ZWPsPijKNcXAHCjJw8rIJocLTE_JPKyieR_SXh6dcBmw_t9Ez2-7YxmkEC739zYDsHr2uxotjFx8YTg_HF9D6E9aWTtpdXNCovXVfZaWZRLqlCcQCaJQ3HfAGHtEIXnfw1C0FL2LAbD0PDSE)
>
>Source des commandes utilisées :
>
> - Site officiel de Gitea, download from binary : [https://docs.gitea.com](https://docs.gitea.com/)

  
  

**Mise à jour du binaire du service *Gitea* :** <a id="7.3"></a>
-

1. Quelle version du binaire avez-vous installée ? Donnez la version et la commande permettant d’obtenir cette information.
    

>Nous avons installé la version 1.21.8. Pour le savoir, la commande est la suivante : 
> ```bash
> gitea --version.
>```
>![](https://lh7-us.googleusercontent.com/uaWC0SRpXiIhXP1OORDep-L3cHkWfPvsAfB5DN5FTHZjXu16XgYCW04wyRM8Z-qL5BpvyBLzkf-ZY9cUohmFvTyL1VtNybLxBESQ0IFUCwcAsshtFnRx0zuE7KyfLHB4jsMzPgUSnr8OUH8zVg2I4p4)

2. Comment faire pour mettre à jour le binaire de votre service sans devoir tout reconfigurer ?

> Pour mettre Gitea à jour sans sans reconfiguration, on peut utiliser la commande : wget [lien vers le fichier de la version souhaitée]
>```bash
>sudo [lien vers le fichier de la version souhaitée] /usr/local/bin/gitea
>```
>```bash
> sudo service gitea start
> ```
3.   Essai en mettant à jour vers la version 1.22-dev :
>```bash
> wget https://dl.gitea.com/gitea/1.22-dev/gitea-1.22-dev-linux-amd64
>```
> ```bash
>sudo mv gitea-1.22-dev-linux-amd64 /usr/local/bin/gitea
>```
>```bash
> sudo service gitea start
>```
